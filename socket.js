const express = require('express');
const app = express();
const port = process.env.PORT || 1337;
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const path = require('path');
const http = require('http');


app.use('/assets',express.static(__dirname + '/public') );

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
});

io.on('connection', function(client) {
    console.log('Client Connected to server');
    setInterval(async ()=>{
    	http.get('http://www.randomtext.me/api/', function(resp){
				let data = '';
				resp.on('data', (chunk)=>{
					data+=chunk;
				});
				resp.on('end', () => {
					client.emit('data',JSON.parse(data)['text_out']);
				});
			}).on("error", function(e){
				console.log("Got error: " + e.message);
			});
    },3000);
});


server.listen(port, "127.0.0.1", function() {
    console.log('Server started on port:' + port)  
});
